package com.jisken.invoice.dao;

import java.util.List;

import com.jisken.invoice.entity.Invoice;

public interface InvoiceDao {
	
	int addInvoice(Invoice invoice);
	
	List<Invoice> findByInvoiceNumberAndTaxId(String invoiceNumber, String vendorTaxID);
	
	List<Invoice> findAll();
}
