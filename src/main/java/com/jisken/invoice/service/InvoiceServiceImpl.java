package com.jisken.invoice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jisken.invoice.dao.InvoiceDao;
import com.jisken.invoice.entity.Invoice;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceDao invoiceDao;
	
	@Override
	public int addInvoice(Invoice invoice) {
		/*public int addInvoice(String invoiceNumber, String vendorTaxID, String date, String currency, String itemDescription, String untaxedAmount, String taxAmount, String totalAmount) {
		Invoice invoice = new Invoice();
		invoice.setInvoiceNumber(invoiceNumber);
		invoice.setVendorTaxID(vendorTaxID);
		invoice.setDate(date);
		invoice.setCurrency(currency);
		invoice.setItemDescription(itemDescription);
		invoice.setUntaxedAmount(untaxedAmount);
		invoice.setTaxAmount(taxAmount);
		invoice.setTotalAmount(totalAmount);*/
		return invoiceDao.addInvoice(invoice);
	}

	@Override
	public List<Invoice> findByInvoiceNumberAndTaxId(String invoiceNumber, String vendorTaxID) {
		return invoiceDao.findByInvoiceNumberAndTaxId(invoiceNumber, vendorTaxID);
	}

	@Override
	public List<Invoice> findAll() {
		return invoiceDao.findAll();
	}

}
