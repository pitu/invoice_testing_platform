package com.jisken.invoice.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.jisken.invoice.entity.User;

public class UserDaoImpl extends JdbcDaoSupport implements UserDao {

	@Override
	public User findOne(Long userId) {
        String sql = "select id, salt, username, password from User where ID=?";
        List<User> userList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<User>(User.class), userId);
        if(userList.size() == 0) {
            return null;
        }
        return userList.get(0);
	}

	@Override
	public User findByUsername(String username) {
        String sql = "select id, salt, username, password from User where username=?";
        List<User> userList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<User>(User.class), username);
        if(userList.size() == 0) {
            return null;
        }
        return userList.get(0);
	}

}
