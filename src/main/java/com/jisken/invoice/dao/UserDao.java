package com.jisken.invoice.dao;

import com.jisken.invoice.entity.User;

public interface UserDao {
    User findOne(Long userId);
    User findByUsername(String username);
}
