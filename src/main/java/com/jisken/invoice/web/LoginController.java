package com.jisken.invoice.web;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class LoginController {

	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String toLogin() {
		Subject subject = SecurityUtils.getSubject();
		if(subject.isAuthenticated()) {

            //设置 登录信息
            String principal = (String) subject.getPrincipal();
            Session session = subject.getSession();
            session.setAttribute("username", principal);

            return "redirect:/";
		}
		return "login";
	}

    /**
     * @param sname
     * @param spassword
     * @return
     */
    @RequestMapping(value="/login", method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestParam("username") String sname,
                        @RequestParam("password") String spassword) {

        //构建登录数据
        UsernamePasswordToken token = new UsernamePasswordToken(sname, spassword);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
        } catch (IncorrectCredentialsException | UnknownAccountException t) {
        	return "bad";
        }
        
        return "";
    }
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logout() {
    	Subject subject = SecurityUtils.getSubject();
    	subject.logout();
		return "login";
    }
}
