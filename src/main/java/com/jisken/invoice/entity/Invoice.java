package com.jisken.invoice.entity;

public class Invoice {
	int id;
	String invoiceNumber;
	String vendorTaxId;
	String date;
	String currency;
	String itemDescription;
	String untaxedAmount;
	String taxAmount;
	String totalAmount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getVendorTaxId() {
		return vendorTaxId;
	}
	public void setVendorTaxId(String vendorTaxID) {
		this.vendorTaxId = vendorTaxID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getUntaxedAmount() {
		return untaxedAmount;
	}
	public void setUntaxedAmount(String untaxedAmount) {
		this.untaxedAmount = untaxedAmount;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
}
