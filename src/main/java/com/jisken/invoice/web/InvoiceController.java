package com.jisken.invoice.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jisken.invoice.entity.Invoice;
import com.jisken.invoice.service.InvoiceService;

@Controller
@RequestMapping("/invoices")
public class InvoiceController {
	
	@Autowired
	private InvoiceService invoiceService;
	
	/**
     * @param invoice
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/addInvoice", method = RequestMethod.POST)
    public String addInvoice(Invoice invoice) {
        
        try {
        	int result = invoiceService.addInvoice(invoice);

        	if(result < 1) {
        		return "bad";
        	}
        } catch (Exception e) {
        	return "bad";
        }
        
        return "";
    }
    
    /**
     * @param invoiceNumber
     * @param vendorTaxID
     * @return
     */
    @RequestMapping(value="/searchInvoice", method = RequestMethod.POST)
    public ModelAndView serchInvoice(String invoiceNumber, String vendorTaxID) {
        
    	List<Invoice> invoiceList = invoiceService.findByInvoiceNumberAndTaxId(invoiceNumber, vendorTaxID);
    	ModelAndView mv= new ModelAndView("display-all");
    	mv.addObject("invoiceList", invoiceList);
        return mv;
    }
    
    /**
     * @return
     */
    @RequestMapping(value="/findAll", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView findAll() {
        
    	List<Invoice> invoiceList = invoiceService.findAll();
    	ModelAndView mv= new ModelAndView("display-all");
    	mv.addObject("invoiceList", invoiceList);
        return mv;
    }
}
