package com.jisken.invoice.service;

import java.util.List;

import com.jisken.invoice.entity.Invoice;

public interface InvoiceService {
	
	/**
	 * 新增发票
	 * @param invoice
	 * @return
	 */
	int addInvoice(Invoice invoice);
	/*public int addInvoice(String invoiceNumber, String vendorTaxID, String date, String currency, String itemDescription, String untaxedAmount, String taxAmount, String totalAmount);*/
	
	/**
     * 根据供应商TaxID和发票号码查找发票
     * @param invoiceNumber
     * @param vendorTaxID
     * @return
     */
    public List<Invoice> findByInvoiceNumberAndTaxId(String invoiceNumber, String vendorTaxID);
    
    /**
	 * 查询所有发票
	 * @return
	 */
    public List<Invoice> findAll();

}
