package com.jisken.invoice.entity;

public class User {
	private int id;
	private String username;
	private String password;
	private String salt;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String sname) {
		this.username = sname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String spassword) {
		this.password = spassword;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getCredentialsSalt() {
        return username + salt;
    }
}
