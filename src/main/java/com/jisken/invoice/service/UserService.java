package com.jisken.invoice.service;

import com.jisken.invoice.entity.User;

public interface UserService {

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public User findByUsername(String username);

}
