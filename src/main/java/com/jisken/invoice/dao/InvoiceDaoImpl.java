package com.jisken.invoice.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.jisken.invoice.entity.Invoice;

public class InvoiceDaoImpl extends JdbcDaoSupport implements InvoiceDao {

	@Override
	public int addInvoice(Invoice invoice) {
//		String sql = "insert into Invoice (invoice_number, vendor_taxID, date, currency, item_description, untaxed_amount, tax_amount, total_amount) values ("
//				+invoice.getInvoiceNumber()+","+invoice.getVendorTaxID()+","+invoice.getDate()+","+invoice.getCurrency()+","+invoice.getItemDescription()
//				+","+invoice.getUntaxedAmount()+","+invoice.getTaxAmount()+","+invoice.getTotalAmount()+")";
		String sql = "insert into Invoice (invoice_number, vendor_tax_id, date, currency, item_description, untaxed_amount, tax_amount, total_amount) values(?,?,?,?,?,?,?,?)";
		int result = getJdbcTemplate().update(sql,new Object[] {
				invoice.getInvoiceNumber(),
				invoice.getVendorTaxId(),
				invoice.getDate(),
				invoice.getCurrency(),
				invoice.getItemDescription(),
				invoice.getUntaxedAmount(),
				invoice.getTaxAmount(),
				invoice.getTotalAmount()
		});
		return result;
	}

	@Override
	public List<Invoice> findByInvoiceNumberAndTaxId(String invoiceNumber, String vendorTaxID) {
		String sql = "select id, invoice_number, vendor_tax_id, date, currency, item_description, untaxed_amount, tax_amount, total_amount from Invoice ";
		List<Invoice> invoiceList = null;
		if (invoiceNumber != null && !"".equals(invoiceNumber) && vendorTaxID != null && !"".equals(vendorTaxID)) {
			sql = sql + "where invoice_number=? and vendor_tax_id=?";
			invoiceList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Invoice>(Invoice.class), invoiceNumber, vendorTaxID);
		} else if (invoiceNumber != null && !"".equals(invoiceNumber)) {
			sql = sql + "where invoice_number=?";
			invoiceList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Invoice>(Invoice.class), invoiceNumber);
		} else if (vendorTaxID != null && !"".equals(vendorTaxID)) {
			sql = sql + "where vendor_tax_id=?";
			invoiceList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Invoice>(Invoice.class), vendorTaxID);
		}
        return invoiceList;
	}

	@Override
	public List<Invoice> findAll() {
		String sql = "select id, invoice_number, vendor_tax_id, date, currency, item_description, untaxed_amount, tax_amount, total_amount from Invoice ";
		List<Invoice> invoiceList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Invoice>(Invoice.class));
		return invoiceList;
	}

}
